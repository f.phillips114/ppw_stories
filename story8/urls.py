from django.conf.urls import url
from django.urls import path
from .views import *
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.story8, name = 'story8'),
    path('book/', views.bookAPI),
]