from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')
def portfolio(request):
    return render(request,"main/portfolio.html")
def story3(request):
    return render(request,"main/story3.html")
def resume(request):
    return render(request,"main/resume.html")
