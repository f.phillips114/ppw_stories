from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('portfolio', views.portfolio, name = 'portfolio'),
    path('story3', views.story3, name = 'story3'),
    path('resume', views.resume, name = 'resume')
]
