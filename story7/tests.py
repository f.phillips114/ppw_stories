from django.test import TestCase, Client
from django.urls import resolve, reverse
from selenium import webdriver
from . import views
# Create your tests here.

class UnitTestForStory7(TestCase):
    def test_response_page(self):
        response = Client().get(reverse('story7:story7'))
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get(reverse('story7:story7'))
        self.assertTemplateUsed(response, 'story7.html')

    def test_func_page(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.story7)
