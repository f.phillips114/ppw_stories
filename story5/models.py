from django.db import models

# Create your models here.
class Matkul(models.Model):
    course = models.CharField(max_length=30)
    lecturer = models.CharField(max_length=30)
    totalsks = models.CharField(max_length=30)
    desc = models.CharField(max_length=20)
    termsyear = models.CharField(max_length=20)
    room = models.CharField(max_length=20)

    def __str__(self):
        return self.course

