from django import forms


class matkulform(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control'
    }
    course = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Course ', max_length=50, required=True)
    lecturer = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Lecturer ', max_length=50, required=True)
    totalsks = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Total SKS ', max_length=50, required=True)
    desc = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Description ', required=True)
    termsyear = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Terms / Year ', max_length=50, required=True)
    room = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Room Class ', max_length=50, required=True)
