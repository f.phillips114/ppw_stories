from django.http import HttpResponse
from django.shortcuts import render, redirect

from story5.models import Matkul
from .forms import matkulform


# Create your views here.

def post_story5(request):
    if request.method == 'POST':
        form = matkulform(request.POST or None, request.FILES or None)
        response_data = {}
        if form.is_valid():
            response_data['course'] = request.POST['course']
            response_data['lecturer'] = request.POST['lecturer']
            response_data['totalsks'] = request.POST['totalsks']
            response_data['desc'] = request.POST['desc']
            response_data['termsyear'] = request.POST['termsyear']
            response_data['room'] = request.POST['room']

            data_matkul = Matkul(course=response_data['course'],
                                 lecturer=response_data['lecturer'],
                                 totalsks=response_data['totalsks'],
                                 desc=response_data['desc'],
                                 termsyear=response_data['termsyear'],
                                 room=response_data['room'],
                                 )
            data_matkul.save()
            return redirect('/story5/#lihat_jadwal')

        else:
            return redirect('/story5')
    else:
        return redirect('/story5')


def story5(request):
    form_tambah = matkulform()
    data = Matkul.objects.all()
    response = {
        'form_tambah': form_tambah,
        'data': data,
    }
    return render(request, 'story5.html', response)


def detail_jadwal(request, nama_matkul):
    data = Matkul.objects.filter(course = nama_matkul)
    response = {
        'data': data,
    }
    return render(request, 'story5-2.html', response)


def delete_jadwal(request, nama_matkul):
    print(nama_matkul)
    data = Matkul.objects.filter(course = nama_matkul)
    data.delete()
    return redirect('/story5')

